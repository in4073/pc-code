/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <sys/time.h>
#include <curses.h>
#include <string.h>
#include "communication.h"
#include "message_handler.h"
#include "ack_queue.h"
#include "gui.h"
#include "keyboard.h"
#include "sender.h"
#include "status.h"
#include "joystick_handler.h"
#include "boot.h"
#include "timer.h"
#include "main.h"
#include "logger.h"


/*__Author: Eric Camellini__
Main scheduler, runs the mainloop and calls the timed actions. */
int main(int argc, char** argv){

	unsigned char c;
	Message *m;

	struct timeval t_command_period, t_ack_period, t_no_connection_time, t_loop_time;

	start_timer(&t_command_period);
	start_timer(&t_ack_period);
	start_timer(&t_no_connection_time);

	init_program();

	for (;;) {

		start_timer(&t_loop_time);

		if(fpga_connected){
			if (fpga_getchar_nb(&c)) {
				//log the hex message, newline for new message
				if(c == SEPARATOR){
					loghex_newl();
				}
				log_incoming_byte(c);

				if(c == SEPARATOR){ //New incoming message
					start_timer(&t_no_connection_time);
					if((m = receive_message())){
						handle_message(m);
						m = NULL;
					}else{
						print_scroll("Checksum mismatch, message discarded.", YELLOW_BLACK);
					}
				}

			}
		}

		if(kbhit()){
			handle_keyboard_input(getch());
		}

		//Connection timeout
		if(CONNECTION_TIMEOUT != 0) { //SET TO 0 TO DISABLE
			if((timer_value_ms(&t_no_connection_time) > CONNECTION_TIMEOUT) && fpga_connected){
				print_scroll("Connection lost...", RED_BLACK);
				end_program(true);
			}
		}

		//Joystick
		if(timer_value_ms(&t_command_period) > COMMAND_PERIOD){
			handle_joystick();
			send_commands();
			start_timer(&t_command_period);
		}

		//Message resending
		if(timer_value_ms(&t_ack_period) > ACK_TIMEOUT){
			resend_ack_queue();
			//print_ack_queue();
			start_timer(&t_ack_period);
		}

    print_status();

		update_response_time(&t_loop_time);
	}

	end_program(true);
}
