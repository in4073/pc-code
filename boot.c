/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "joystick_handler.h"
#include "communication.h"
#include "gui.h"
#include "logger.h"
#include "status.h"
#include "sender.h"
#include "main.h"

#define INITIAL_DELAY 1 //Seconds
#define FINAL_DELAY 1 //Seconds
#define EXIT_SENDS 3
#define EXIT_RESEND_DELAY 0.1
#define JOYSTICK_INITIAL_CHECKS 10

#define UPLOAD_SUCCESS_STRING "Done!"
#define UPLOAD_SCRIPT "./fpga_upload 2>&1" //Script, with stderr redir to stdout


/*__Author: Eric Camellini__
Function that calls the script to upload the fpga proram and checks
if the operation was successful.*/
bool fpga_upload(){
  FILE *fp;
  char line[256];
  bool success = false;

  fp = popen(UPLOAD_SCRIPT, "r");
  if (fp == NULL) {
    printf("Failed to run the fpga_upload script.\n" );
    exit(1);
  }

  while (fgets(line, sizeof(line), fp) != NULL) {
    printf("%s", line);
    if(strstr(line, UPLOAD_SUCCESS_STRING) != NULL){ //Contains
      success = true;
    }
  }

  /* close */
  pclose(fp);

  return success;
}


/*__Author: Eric Camellini__
This function calls all the operations
that must be performed at program startup */
void init_program(){
  //Fpga: open communication, firmware upload
  open_fpga();
  if(fpga_connected){
    if(!fpga_upload()){
      printf("Failed to upload the qr firmware: program terminated.\n");
      exit(0);
    }
  }else{
    printf("Fpga non connected, running in test mode.\n");
  }
  printf("QR program started.\n");
  //Open joystick communication, check neutral position
  joystick_init();

  if(joystick_connected){
    int i = JOYSTICK_INITIAL_CHECKS;
    while(i != 0){
      handle_joystick();
      i--;
    }
    while(!joystick_neutral){
      printf("Joystick position must be neutral at program startup.\n");
      printf("Put joystick in neutral position, press ENTER to retry or CRTL^C to exit...\n");
      printf("%d, %d, %d, %d\n", lift, roll, pitch, yaw);
      getchar();
      handle_joystick();
    }
  }

  load_status(); //Restore saved control parameters
  init_logger();
  //start_fpga_program();
  printf("Starting application...\n");
  sleep(INITIAL_DELAY);
  gui_init();
  send_params();
}


/*__Author: Eric Camellini__
This function calls all the operations
that must be performed at program exit, after setting the
QR in safe mode. */
void exit_program(){
  print_scroll("Terminating QR program...", RED_BLACK);
  int i;
  for(i = 0; i < EXIT_SENDS; i++){
    change_mode(EXIT_MODE);
    sleep(EXIT_RESEND_DELAY);
  }
  sleep(FINAL_DELAY);
  print_scroll("Program terminated, press a key to exit.", RED_BLACK);
  gui_exit();
  close_fpga();
  close_joystick();
  exit_logger();
  save_status();
  exit(0);
}


/*__Author: Eric Camellini__
This function calls the exit function if the QR is in safe
mode. It is called when the user manually exits the program
using the proper button. */
void end_program(bool conn_lost){
  if(conn_lost){
    exit_program();
  }else if(fpga_state == SAFE_MODE){
    exit_program();
  }else{
    print_scroll("Go to safe mode before terminating the program.\n", RED_BLACK);
  }
}
