/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include "status.h"
#include "gui.h"
#include "keyboard.h"

#define STATUS_FILE_PATH "saved_status.csv"
#define SAVED_VALUES 5 /*p p1 p2*/

FILE *status_file;

//Control parameters
uint16_t p = P_INIT;
uint16_t p1 = P1_INIT;
uint16_t p2 = P2_INIT;
uint16_t c1 = C1_INIT;
uint16_t c2 = C2_INIT;

/*__Author: Eric Camellini__
Loads the saved control parameters if the corresponding file exists. */
void load_status(){
  char line[8];

  int values[SAVED_VALUES];
  status_file = fopen(STATUS_FILE_PATH,"r");
  int i = 0;
  if(status_file != NULL){
    while(fgets(line, sizeof(line), status_file) != NULL){
      values[i++] = atoi(line);
    }
    p = values[0];
    p1 = values[1];
    p2 = values[2];
    c1 = values[3];
    c2 = values[4];
    fclose(status_file);
  }
}


/*__Author: Eric Camellini__
Saves the current control parameters in a file. */
void save_status(){
  status_file = fopen(STATUS_FILE_PATH,"w+");
  if(status_file == NULL){
    print_scroll("Error saving status.", YELLOW_BLACK);
  } else {
    fprintf(status_file, "%d\n%d\n%d\n%d\n%d\n", p, p1, p2, c1, c2);
  }
  fclose(status_file);
}


/*__Author: Eric Camellini__
Increment the trim variable checking its boundaries. */
int8_t trim_up(int8_t trim){
  if(trim <= MAX_COMMAND - KB_CHANGE_COMMAND){
    trim += KB_CHANGE_COMMAND;
  } else {
    trim = MAX_COMMAND;
  }
  return trim;
}


/*__Author: Eric Camellini__
Decrement the trim variable checking its boundaries. */
int8_t trim_down(int8_t trim){
  if(trim >= MIN_COMMAND + KB_CHANGE_COMMAND){
    trim -= KB_CHANGE_COMMAND;
  } else {
    trim = MIN_COMMAND;
  }
  return trim;
}


/*__Author: Eric Camellini__
Increment the lift trim variable checking its boundaries. */
void lift_trim_up(){
  if(lift_trim <= MAX_LIFT - KB_CHANGE_COMMAND){
    lift_trim += KB_CHANGE_COMMAND;
  } else {
    lift_trim = MAX_LIFT;
  }
}


/*__Author: Eric Camellini__
Increment the lift trim variable checking its boundaries. */
void lift_trim_down(){
  if(lift_trim >= MIN_LIFT + KB_CHANGE_COMMAND){
    lift_trim -= KB_CHANGE_COMMAND;
  } else {
    lift_trim = MIN_LIFT;
  }
}


/*__Author: Eric Camellini__
Increment the control param variable par checking its boundaries. */
uint16_t param_up(uint16_t par){
  if(par <= MAX_PARAM - KB_CHANGE_PARAMS){
    par += KB_CHANGE_PARAMS;
  } else {
    par = MAX_PARAM;
  }
  return par;
}


/*__Author: Eric Camellini__
Decrement the control param variable par checking its boundaries. */
uint16_t param_down(uint16_t par){
  if(par >= MIN_PARAM + KB_CHANGE_PARAMS){
    par -= KB_CHANGE_PARAMS;
  } else {
    par = MIN_PARAM;
  }
  return par;
}
