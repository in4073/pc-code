#ifndef MESSAGE_HANDLER_H
#define MESSAGE_HANDLER_H

#include "protocol.h"

void handle_message(Message *m);
void print_telemetry();

#endif
