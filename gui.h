#ifndef GUI_H
#define GUI_H

#define STATUS_X 60
#define STATUS_Y 1
#define STATUS_WIDTH 80
#define STATUS_HEIGHT 20

#define SCROLL_X 1
#define SCROLL_Y 1
#define SCROLL_WIDTH 50
//#define SCROLL_HEIGHT 20

#define RED_BLACK 1
#define YELLOW_BLACK 2
#define GREEN_BLACK 3
#define CYAN_BLACK 4


void gui_init();
void gui_exit();
int kbhit();
void print_status();
void print_scroll(char *fmt, int color_pair_id, ...);

#endif
