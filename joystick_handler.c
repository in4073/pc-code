/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "joystick.h"
#include "status.h"
#include "gui.h"
#include "boot.h"

#define JOYSTICK "/dev/input/js0"

#define N_16 65536
#define LIFT_CUT 5
#define N_8 256

#define A_LIFT 3
#define A_ROLL 0
#define A_PITCH 1
#define A_YAW 2
#define B_FIRE 0

uint16_t lift = 0;
int8_t roll = 0;
int8_t pitch = 0;
int8_t yaw = 0;

bool joystick_connected = false;
bool joystick_neutral = false;

int	axis[6];
int	button[12];

struct js_event js;
int fd_joy;


/*__Author: Eric Camellini__
Initializes the joystick descriptor. */
void joystick_init(){
  if ((fd_joy = open(JOYSTICK, O_RDONLY)) >= 0) {
    joystick_connected = true;
    fcntl(fd_joy, F_SETFL, O_NONBLOCK);
  }
}


/*__Author: Eric Camellini__
Maps the lift readings from the joystick
to the correct value to send to the QR */
uint16_t lift_pre_process(int axis_value){
  return (-(axis_value/2 + 1) + N_16/4) >> LIFT_CUT;
}


/*__Author: Eric Camellini__
Fills the global lift, roll, pitch, yaw variables with the
current joystick readings */
void handle_joystick(){
  if(joystick_connected){
    while (read(fd_joy, &js, sizeof(struct js_event)) == sizeof(struct js_event))  {

      // register data
      switch(js.type & ~JS_EVENT_INIT) {
      case JS_EVENT_BUTTON:
        button[js.number] = js.value;
        break;
      case JS_EVENT_AXIS:
        axis[js.number] = js.value;
        break;
      }
    }

    if (errno != EAGAIN) {
      print_scroll("joystick: error reading (EAGAIN)", YELLOW_BLACK);
    } else {

      //print_joystick_readings();
      lift = lift_pre_process(axis[A_LIFT]);
      roll = -axis[A_ROLL]/N_8;
      pitch = axis[A_PITCH]/N_8;
      yaw = axis[A_YAW]/N_8;

      if((lift != 0) || (yaw != 0) || (pitch != 0) || (roll != 0)){
        joystick_neutral = false;
      } else {
        joystick_neutral =  true;
      }

      if(button[B_FIRE]){
        end_program(false);
      }
    }

  }
}


/*__Author: Eric Camellini__
Closes the joistick descriptor. */
void close_joystick(){
  close(fd_joy);
}
