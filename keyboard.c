/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <curses.h>
#include <stdlib.h>
#include "status.h"
#include "sender.h"
#include "gui.h"
#include "boot.h"
#include "main.h"

#define ESC 27
#define IS_ARROW '['
#define ARROW_UP 'A'
#define ARROW_DOWN 'B'
#define ARROW_RIGHT 'C'
#define ARROW_LEFT 'D'

uint16_t lift_trim = 0;
int8_t roll_trim = 0;
int8_t pitch_trim = 0;
int8_t yaw_trim = 0;

int mode = 0;


/*__Author: Eric Camellini__
Handles a key press. */
void handle_keyboard_input(int ch){
  switch(ch){
    case -1:
    break;

    //ESC OR ARROWS
    case ESC:
    if(getch() != IS_ARROW){
      end_program(false);
    } else switch(getch()){

      //ARROWS
      case ARROW_UP:
        //Arrow up
        pitch_trim = trim_down(pitch_trim);
        send_commands();
        break;

      case ARROW_DOWN:
        //Arrow down
        pitch_trim = trim_up(pitch_trim);
        send_commands();
        break;

      case ARROW_RIGHT:
        //Arrow right
        roll_trim = trim_down(roll_trim);
        send_commands();
        break;

      case ARROW_LEFT:
        //Arrow left
        roll_trim =  trim_up(roll_trim);
        send_commands();
        break;
    }
    break;

    //MODES
    case '0':
      change_mode(SAFE_MODE);
      break;

    case '1':
      change_mode(PANIC_MODE);
      break;

    case '2':
      change_mode(MANUAL_MODE);
      break;

    case  '3':
      change_mode(CALIBRATION_MODE);
      break;

    case  '4':
      change_mode(YAW_CONTROL_MODE);
      break;

    case  '5':
      change_mode(FULL_CONTROL_MODE);
      break;

    //LOGGING
    case  '8':
      start_logging();
      break;

    case  '9':
      log_request();
      break;

    //LIFT, YAW
    case 'a':
      lift_trim_up();
      send_commands();
      break;

    case 'z':
      lift_trim_down();
      send_commands();
      break;

    case 'q':
      yaw_trim = trim_down(yaw_trim);
      send_commands();
      break;

    case 'w':
      yaw_trim = trim_up(yaw_trim);
      send_commands();
      break;

      //CONTROL PARAMS
    case 'u':
      p = param_up(p);
      send_params();
      break;

    case 'j':
      p = param_down(p);
      send_params();
      break;

    case 'i':
      p1 = param_up(p1);
      send_params();
      break;

    case 'k':
      p1 = param_down(p1);
      send_params();
      break;

    case 'o':
      p2 = param_up(p2);
      send_params();
      break;

    case 'l':
      p2 = param_down(p2);
      send_params();
      break;

    //Params extension
    case 'y':
      c1 = param_up(c1);
      send_params();
      break;

    case 'h':
      c1 = param_down(c1);
      send_params();
      break;

    case 't':
      c2 = param_up(c2);
      send_params();
      break;

    case 'g':
      c2 = param_down(c2);
      send_params();
      break;

    /*
    case 't':
      //Testing button:
      break;
    */

      //OTHER BUTTONS
    default:
        print_scroll("Invalid key: %d.", YELLOW_BLACK, ch);
        break;
  }
}
