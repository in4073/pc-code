#ifndef BOOT_H
#define BOOT_H

#include <stdbool.h>

void init_program();
void end_program(bool conn_lost);

#endif
