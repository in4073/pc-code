#ifndef JOYSTICK_HANDLER_H
#define JOYSTICK_HANDLER_H

void joystick_init();
void handle_joystick();
void close_joystick();

#endif
