/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <termios.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/time.h>
#include "protocol.h"
#include "communication.h"
#include "ack_queue.h"
#include "gui.h"
#include "logger.h"

/*------------------------------------------------------------
 * Communication setup
 *------------------------------------------------------------
 */
#define START_FPGA_BYTE "s"

int fd_fpga;
bool fpga_connected = false;


/*__Author: Eric Camellini__
Opens RS232 channel to communicate with the fpga, and sets its parameters*/
void open_fpga(){
	struct termios	tty;

	//Fpga serial communication
	fd_fpga = open(USB, O_RDWR | O_NOCTTY);

	if(fd_fpga >=0){
		fpga_connected = true;

		tcgetattr(fd_fpga, &tty);

		tty.c_iflag = IGNBRK;
		tty.c_oflag = 0;
		tty.c_lflag = 0;

		tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
		tty.c_cflag |= CLOCAL | CREAD;
		cfsetospeed(&tty, B115200);
		cfsetispeed(&tty, B115200);

		tty.c_cc[VMIN]  = 0;
		tty.c_cc[VTIME] = 0;

		tty.c_iflag &= ~(IXON|IXOFF|IXANY);

		tcsetattr (fd_fpga, TCSANOW, &tty); // non-canonical

		tcflush(fd_fpga, TCIOFLUSH); // flush I/O buffer
	}
}

/*__Author: Eric Camellini__
Sends the start byte to the fpga.
The program must already be uploaded. */
void start_fpga_program(){
	write(fd_fpga, START_FPGA_BYTE, 1);
	write(fd_fpga, "\r", 1);
}

/*__Author: Eric Camellini__
Closes the RS232 fpga channel.*/
void close_fpga(){
	close(fd_fpga);
}


/*------------------------------------------------------------
 * Fpga readings
 *------------------------------------------------------------
 */


/*__Taken from the terminal example code__
Reads a byte sent by the fpga over the RS232 channel in a non blocking way.*/
int	fpga_getchar_nb(unsigned char *c)
{
	int result;
	result = read(fd_fpga, c, 1);

	if(result == -1){
		exit(0);
	}
	else{
		return result;
	}
}


/*__Author: Eric Camellini__
Reads a byte sent by the fpga over the RS232 channel in a blocking way,
with a timeout. Returns -1 if no char is received before the timeout. */
struct timeval t_start, current;
double blocking_time;
#define CHAR_TIMEOUT 200

int fpga_getchar()
{
	gettimeofday(&t_start, NULL);
	unsigned char c;
	while (fpga_getchar_nb(&c) <= 0){
		gettimeofday(&current, NULL);
		blocking_time = (current.tv_sec - t_start.tv_sec) * 1000.0;
		blocking_time += (current.tv_usec - t_start.tv_usec) / 1000.0;
		if(blocking_time > CHAR_TIMEOUT){
			return -1;
		}
	}
	log_incoming_byte(c);
	return c;
}


/*------------------------------------------------------------
 * Message sending and receiving
 *------------------------------------------------------------
 */


/*__Author: Eric Camellini__
Sends the message payload, performing endiannes handling if needed. */
void send_payload(Message *m){
	int i;
	unsigned char *p;

  switch (m->type) {
		case 0:
		break;

  	case MESSAGE_PARAM:
		  p = (unsigned char *) m->payload;
		  for(i=0; i < payload_size[m->type]; i = i+2) {
			  write(fd_fpga, &p[i+1], 1);
			  write(fd_fpga, &p[i], 1);
		  }
		break;

		case MESSAGE_CONTROL:
			p = (unsigned char *) m->payload;
			write(fd_fpga, &p[1], 1); //Lift is 2 bytes, big endian conversion
			write(fd_fpga, &p[0], 1);
			for(i=2; i < payload_size[m->type]; i++) { //WRITE IN BIG ENDIAN
				write(fd_fpga, &p[i], 1);
			}
			break;

		default:
		  p = (unsigned char *) m->payload;
		  for(i=0; i < payload_size[m->type]; i++) { //WRITE IN BIG ENDIAN
				write(fd_fpga, &p[i], 1);
			}
		break;
  }
}


/*__Author: Eric Camellini__
 Sends the message struct pointed by *m */
void send_message(Message *m, int ack_enqueue){
	unsigned char sep = SEPARATOR;
	if(fpga_connected){
		write(fd_fpga, &sep, 1);
		write(fd_fpga, &m->type, 1);
		send_payload(m);
		write(fd_fpga, &m->check, 1);

		if(ack_enqueue){
			insert_into_ack_queue(m);
		}
	}
}


/*__Author: Eric Camellini__
This function reads the payload in the correct way,
handling also the endiannes for the various integer values */
void *read_payload(int type){
	int size = payload_size[type];
	unsigned char *payload = malloc(size);
	int i;

	switch (type) {

		case MESSAGE_LOG_SENSORS:
			for(i=0; i < size; i = i+4) { //READ IN LITTLE ENDIAN
				payload[i + 3] = fpga_getchar();
				payload[i + 2] = fpga_getchar();
				payload[i + 1] = fpga_getchar();
				payload[i + 0] = fpga_getchar();
			}
		break;

		case MESSAGE_LOG_TELEMETRY:
			payload[3] = fpga_getchar();
			payload[2] = fpga_getchar();
			payload[1] = fpga_getchar();
			payload[0] = fpga_getchar();
			payload[5] = fpga_getchar();
			payload[4] = fpga_getchar();
			for(i=6; i < size; i++) {
				payload[i] = fpga_getchar();
			}
		break;

		case MESSAGE_TELEMETRY:
			for(i=0; i < 8; i = i+4) { //READ IN LITTLE ENDIAN
				payload[i + 3] = fpga_getchar();
				payload[i + 2] = fpga_getchar();
				payload[i + 1] = fpga_getchar();
				payload[i + 0] = fpga_getchar();
			}
			for(i=8; i < size; i = i+2) { //READ IN LITTLE ENDIAN
				payload[i+1] = fpga_getchar();
				payload[i] = fpga_getchar();
			}
		break;

		default:
		  for(i=0; i < size; i++)
			  payload[i] = fpga_getchar();
		  break;
	}

	return payload;
}


/*__Author: Eric Camellini__
 Reads the bytes of an incoming message, puts it in a Message struct and
  returns a pointer to it */
Message *receive_message(){
	Message *m = malloc(sizeof(Message));
	m->type = fpga_getchar();
	m->payload = read_payload(m->type);
	unsigned char checksum = fpga_getchar();
  m->check = calculate_checksum(m);
	if(m->check != checksum){
		print_scroll("Checksum %d, %d", YELLOW_BLACK, m->check, checksum);
		fprintf(log_hex, " <- %02x != %02x",  m->check, checksum);
		//dump_msg(m);
		return NULL; //Checksum mismatch
	}else{
		return m;
	}
}
