/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/

#include <stdlib.h>
#include "protocol.h"


/*
Size of the payloads, must be addressed using payload_size[msg-type] */
int payload_size[MESSAGE_TYPES + 1] = {0 /*nothing*/,
																			 PAYLOAD_CONTROL_SIZE,
																			 PAYLOAD_MODE_SIZE,
																			 PAYLOAD_PARAM_SIZE,
																			 PAYLOAD_DEBUG_SIZE,
																			 PAYLOAD_TELEMETRY_SIZE,
																			 PAYLOAD_ACK_SIZE,
																			 PAYLOAD_START_LOG_SIZE,
																		   PAYLOAD_REQUEST_LOG_SIZE,
																		 	 PAYLOAD_LOG_SENSORS_SIZE,
																		 	 PAYLOAD_LOG_TELEMETRY_SIZE};


/*__Author: Eric Camellini__
Creates a new PayloadControl structure and returns a pointer to it.*/
PayloadControl *new_payload_control(uint16_t lift, int8_t roll, int8_t pitch, int8_t yaw){
	PayloadControl *m = (PayloadControl *) malloc(sizeof(PayloadControl));
	m->lift = lift;
	m->roll = roll;
	m->pitch = pitch;
	m->yaw = yaw;
	return m;
}


/*__Author: Eric Camellini__
Creates a new PayloadMode structure and returns a pointer to it.*/
PayloadMode *new_payload_mode(uint8_t mode){
  PayloadMode *p = (PayloadMode *) malloc(sizeof(PayloadMode));
	p->mode = mode;
	return p;
}


/*__Author: Eric Camellini__
Creates a new PayloadParam structure and returns a pointer to it.*/
PayloadParam *new_payload_param(uint16_t p, uint16_t p1, uint16_t p2, uint16_t c1, uint16_t c2){
	PayloadParam *m = (PayloadParam *) malloc(sizeof(PayloadParam));
	m->p = p;
	m->p1 = p1;
	m->p2 = p2;
	m->c1 = c1;
	m->c2 = c2;
	return m;
}


/*__Author: Eric Camellini__
Calculates the checksum of a the message ponted by *m.
http://www.linuxquestions.org/questions/programming-9/calculating-checksum-of-a-structure-275434/*/
unsigned char calculate_checksum(Message *m){
	  unsigned char *p = (unsigned char *) m->payload;
		int i;
		unsigned char checksum = m->type;
		for(i=0; i < payload_size[m->type]; i++)
			checksum += p[i];
		return checksum;
}


/*__Author: Eric Camellini__
Creates a new message structure and returns a pointer to it.*/
Message *new_message(uint8_t type, void *payload){
	Message *m = (Message *) malloc(sizeof(Message));
	m->type = type;
	m->payload = payload;
	m->check = calculate_checksum(m);
	return m;
}
