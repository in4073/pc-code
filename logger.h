#include <stdio.h>

#ifndef LOGGER_H
#define LOGGER_H

#include "protocol.h"

void init_logger();
void log_sensors_line(PayloadLogSensors *p);
void log_telemetry_line(PayloadLogTelemetry *p);
void log_console_line(char *str);
void log_incoming_byte(unsigned char c);
void loghex_newl();
void exit_logger();

extern FILE *log_hex;

#endif
