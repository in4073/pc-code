#ifndef TIMER_H
#define TIMER_H

void start_timer(struct timeval *t);
double timer_value_ms(struct timeval *t);
double timer_value_us(struct timeval *t);
void update_response_time(struct timeval *s);

#endif
