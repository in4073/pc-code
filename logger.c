/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <stdio.h>
#include <sys/stat.h>
#include "protocol.h"
#include "gui.h"

#define LOG_SENSORS_PATH "logs/log_sensors.csv"
#define LOG_CONSOLE_PATH "logs/log_console.txt"
#define LOG_HEX_PATH "logs/log_hex.txt"
#define LOG_TELEMETRY_PATH "logs/log_telemetry.csv"

FILE *log_sensors;
FILE *log_console;
FILE *log_hex;
FILE *log_telemetry;

int log_sensors_index = 0;

int log_telemetry_index = 0;

/*__Author: Eric Camellini__
Initializes the log files. */
void init_logger(){
  mkdir("logs", S_IRWXU | S_IRWXG | S_IRWXO);
  log_sensors = fopen(LOG_SENSORS_PATH,"w+");
  if(log_sensors == NULL){
    print_scroll("Error opening sensors log file.", YELLOW_BLACK);
  }
  log_console = fopen(LOG_CONSOLE_PATH,"w+");
  if(log_console == NULL){
    print_scroll("Error opening console log file.", YELLOW_BLACK);
  }
  log_hex = fopen(LOG_HEX_PATH,"w+");
  if(log_hex == NULL){
    print_scroll("Error opening hex log file.", YELLOW_BLACK);
  }
  log_telemetry = fopen(LOG_TELEMETRY_PATH,"w+");
  if(log_telemetry == NULL){
    print_scroll("Error opening telemetry log file.", YELLOW_BLACK);
  }
}

/*__Author: Eric Camellini__
Function to convert a fixed point value with a certain precision into a
floating point value. */
float fixed_to_float(int fp, int precision){
  return (float) (fp/((2^precision)-1));
}

/*__Author: Eric Camellini__
Writes a sensor log line in the respective log file. */
void log_sensors_line(PayloadLogSensors *p){
  //print_scroll("Printing sensors log line number %d.", GREEN_BLACK, log_sensors_index);
  fprintf(log_sensors, "%d,", p->timestamp);
  fprintf(log_sensors, "%d,", p->s1);
  fprintf(log_sensors, "%d,", p->s2);
  fprintf(log_sensors, "%d,", p->s3);
  fprintf(log_sensors, "%d,", p->s4);
  fprintf(log_sensors, "%d,", p->s5);
  fprintf(log_sensors, "%d\n", p->s6);
  log_sensors_index++;
}


/*__Author: Eric Camellini__
Writes a telemetry log line in the respective log file. */
void log_telemetry_line(PayloadLogTelemetry *p){
  //print_scroll("Printing telemetry log line number %d.", GREEN_BLACK, log_telemetry_index);
  fprintf(log_telemetry, "%d,", p->timestamp);
  fprintf(log_telemetry, "%d,", p->mode);
  fprintf(log_telemetry, "%d,", p->rec_lift);
  fprintf(log_telemetry, "%d,", p->rec_roll);
  fprintf(log_telemetry, "%d,", p->rec_pitch);
  fprintf(log_telemetry, "%d,", p->rec_yaw);
  fprintf(log_telemetry, "%d,", p->ae1);
  fprintf(log_telemetry, "%d,", p->ae2);
  fprintf(log_telemetry, "%d,", p->ae3);
  fprintf(log_telemetry, "%d\n", p->ae4);
  log_telemetry_index++;
}

/*__Author: Eric Camellini__
Writes a console log line in the respective file.*/
void log_console_line(char *str){
  fprintf(log_console, "%s\n",str);
}


/*__Author: Eric Camellini__
Writes a byte represented as an Hex value into the hex log file.*/
void log_incoming_byte(unsigned char c){
  fprintf(log_hex, "%02X ", c);
}

void loghex_newl() {
	fprintf(log_hex, "\n");
}

/*__Author: Eric Camellini__
Closes the log files. */
void exit_logger(){
  fclose(log_sensors);
  fclose(log_console);
  fclose(log_hex);
  fclose(log_telemetry);
}
