#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "protocol.h"

#define USB "/dev/ttyUSB0"
#define CONNECTION_TIMEOUT 2000 //ms

#define ACK 1
#define NO_ACK 0

void	term_initio();
void	term_exitio();
void open_fpga();
void close_fpga();
int	fpga_getchar_nb(unsigned char *c);
int fpga_getchar();
void send_message(Message *m, int ack_enqueue);
Message *receive_message();
void start_fpga_program();

#endif
