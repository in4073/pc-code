/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <curses.h>
#include <locale.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include "gui.h"
#include "status.h"
#include "protocol.h"
#include "logger.h"

int n_scroll = 0;

WINDOW *scroll_win;
WINDOW *win_status;

int win_x;
int win_y;

const char* fpga_states[] = {"Safe mode", "Panic mode", "Manual mode", "Calibration mode", "Yaw control mode", "Full control mode", "Send log mode", "Exit mode"};

/*__Author: Eric Camellini__
Initializes the GUI. */
void gui_init(){
  initscr();
  cbreak(); //raw() to disable CTRL-c or CTRL-z
  noecho();
  curs_set(FALSE);
  setlocale(LC_ALL, "");
  nodelay(stdscr, true);
  start_color();
  init_pair(RED_BLACK, COLOR_RED/*fg*/, COLOR_BLACK/*bg*/);
  init_pair(YELLOW_BLACK, COLOR_YELLOW/*fg*/, COLOR_BLACK/*bg*/);
  init_pair(GREEN_BLACK, COLOR_GREEN/*fg*/, COLOR_BLACK/*bg*/);
  init_pair(CYAN_BLACK, COLOR_CYAN/*fg*/, COLOR_BLACK/*bg*/);

  getmaxyx(stdscr, win_y, win_x);
  scroll_win = newwin(win_y, SCROLL_WIDTH, SCROLL_Y, SCROLL_X);
  win_status = newwin(STATUS_HEIGHT, STATUS_WIDTH, STATUS_Y, STATUS_X);
}


/*__Author: Eric Camellini__
Restores the terminal default status and closes the GUI. */
void gui_exit(){
  nodelay(stdscr, false);
	getch();
  endwin();
}


//http://stackoverflow.com/questions/4025891/create-a-function-to-check-for-key-press-in-unix-using-ncurses
/*Checks if a key was pressed. */
int kbhit(){
    int ch = getch();

    if(ch != ERR){
      ungetch(ch);
      return 1;
    } else {
      return 0;
    }
}


/*__Author: Eric Camellini__
Refreshes the scroll window, clears it if it is full. */
void update_scroll(){
  wrefresh(scroll_win);
  n_scroll++;
  if(n_scroll == (win_y - 1)){
    n_scroll = 0;
    wclear(scroll_win);
  }
}


/*__Author: Eric Camellini__
Prints the format string pointed by *str to the scroll window, with the colors
indicated by the color_pair_id (ids are defined in gui.h). The remaining
parameters are the ones for the formatting of the string. */
void print_scroll(char *fmt, int color_pair_id, ...){

  va_list ap;
  va_start(ap, color_pair_id);

  char buf[SCROLL_WIDTH];
  vsprintf(buf, fmt, ap);
  va_end(ap);

  wattron(scroll_win, COLOR_PAIR(color_pair_id));
  mvwprintw(scroll_win, n_scroll, 0, buf);
  wattroff(scroll_win, COLOR_PAIR(color_pair_id));

  update_scroll();
  log_console_line(buf);
}


/*__Author: Eric Camellini__
Prints the status of the system in the status window. */
void print_status(){
  //Readings and pc code status
  mvwprintw(win_status,0,0,"Lift tot: %u, Roll tot: %d, Pitch tot: %d, Yaw tot: %d\n\r",tot_lift, tot_roll, tot_pitch, tot_yaw);
	mvwprintw(win_status,1,0,"Lift: %u, Roll: %d, Pitch: %d, Yaw: %d\n\r",lift, roll, pitch, yaw);
	mvwprintw(win_status,2,0,"Lift trim: %u, Roll trim: %d, Pitch trim: %d, Yaw trim: %d\n\r", lift_trim, roll_trim, pitch_trim, yaw_trim);
	mvwprintw(win_status,3,0,"p: %.2f, p1: %.2f, p2: %.2f, c1: %d, c2: %d\n\r", (float)p/100, (float)p1/100, (float)p2/100, c1, c2);
	mvwprintw(win_status,4,0,"Mode: %d\n\r", mode);

  //Connection info
 	mvwprintw(win_status,6,0,"Fpga Connected: %s\n\r", fpga_connected == 1 ? "Yes" : "No");
	mvwprintw(win_status,7,0,"Joystick Connected: %s\n\r", joystick_connected ? "Yes" : "No");

  //Fpga telemetry data
  mvwprintw(win_status,9,0,"Fpga state: %d (%s)\n\r", fpga_state, fpga_states[fpga_state]);
  mvwprintw(win_status,10,0,"Avg loop time: %d us, Max loop time: %d us\n\r", avg_control_loop_time, max_control_loop_time);
  mvwprintw(win_status,11,0,"Loop misses: %d", loop_misses);
  mvwprintw(win_status,12,0,"Avg system response: %d us, Max system response: %d us\n\r", avg_system_response, max_system_response); //To Do
  mvwprintw(win_status,13,0,"Roll response: %d\n\r", roll_response);
  mvwprintw(win_status,14,0,"Pitch response: %d\n\r", pitch_response);
  mvwprintw(win_status,15,0,"Yaw response: %d\n\r", yaw_response);
  mvwprintw(win_status,16,0,"ae1: %d, ae2: %d, ae3: %d, ae4: %d\n\r", ae1, ae2, ae3, ae4);
	wrefresh(win_status);
}


/*__Author: Eric Camellini__
Prints the bytes of a the message pointed by *m using hexadecimal notation.
http://www.linuxquestions.org/questions/programming-9/calculating-checksum-of-a-structure-275434/*/
void dump_msg(Message *m){
	unsigned char *p = (unsigned char *) m->payload;
  int i;
	print_scroll("TYPE: %02X", GREEN_BLACK, m->type);
	print_scroll("PAYLOAD:", GREEN_BLACK);
	for(i=0; i<payload_size[m->type]; i++){
    //if((i&7)==0) printf("\n\r");
    print_scroll("%02X", GREEN_BLACK, p[i]);
  }
	print_scroll("CHECKSUM: %02X", GREEN_BLACK, m->check);
}
