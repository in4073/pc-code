/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/
#include <stdlib.h>
#include <sys/time.h>
#include "status.h"
#include "main.h"


#define N_MEASUREMENTS 64 //ms
#define COMMUNICATION_TIME 1.2 //ms

int avg_system_response = 0;
int max_system_response = 0;

/* The max and avg system response time are based on the last 64 measurements. */
double time_measurements[N_MEASUREMENTS];
int current_measurement = 0;

double sum = 0.0;
struct timeval current;


/*__Author: Eric Camellini__
Measures the current timeofday and puts it into t.
It can be used to start or restart a timer. */
void start_timer(struct timeval *t){
  gettimeofday(t, NULL);
}


/*__Author: Eric Camellini__
Returns the time elapsed from when t was started, in milliseconds */
double timer_value_ms(struct timeval *t){
  gettimeofday(&current, NULL);
  double ret;
  ret = (current.tv_sec - t->tv_sec) * 1000.0;
  ret += (current.tv_usec - t->tv_usec) / 1000.0;
  return ret;
}


/*__Author: Eric Camellini__
Returns the time elapsed from when t was started, in microseconds. */
double timer_value_us(struct timeval *t){
  return timer_value_ms(t)*1000;
}


/*__Author: Eric Camellini__
Updates the avg and max system response time (expressed in us),
and to do it it takes into account:
 - The main loop time;
 - The control loop time measured on the fpga (avg and max, recived with the telemetry);
 - The period of the commands sending action (In the worst case it will be the
     time that elapses from a joystick movement to the commands sending, on average
     is the half);
 - The avg time that a message takes to reach the qr. This is an estimation: we
     know that we can transmit  10 bytes each 2 ms (assignment, pag. 5) and the
     avg message size is the size of the command message (because it is
     sent periodically and more frequently than the other messages),
     so 6 bytes (M, lift, roll, pitch, yaw, checksum) that results in 1.2ms. */
void update_response_time(struct timeval *s){
  double t = timer_value_us(s);
  sum -= time_measurements[current_measurement];
	time_measurements[current_measurement] = t;
  sum += t;

  int i;
  double max = 0.0;
	for(i = 0; i < N_MEASUREMENTS; i++){
		if(time_measurements[i] > max){
			max = time_measurements[i];
		}
	}

	max_system_response = (int) max + COMMUNICATION_TIME*1000 + max_control_loop_time + (COMMAND_PERIOD * 1000);
	avg_system_response = (int) sum/N_MEASUREMENTS + COMMUNICATION_TIME*1000 + avg_control_loop_time + (COMMAND_PERIOD * 1000)/2;

  current_measurement++;
	if(current_measurement == N_MEASUREMENTS - 1){
		current_measurement = 0;
	}
}
