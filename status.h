#ifndef STATUS_H
#define STATUS_H

/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/

/*This file contains the status of the program: all the variables are
extern, defined and initialized in other files.
It is sufficient to include this file in order to access or modify them. */
#include <stdint.h>
#include <stdbool.h>

#define MAX_COMMAND 127
#define MIN_COMMAND -128
#define MAX_LIFT 1023
#define MIN_LIFT 0
#define MAX_PARAM 65535
#define MIN_PARAM 0

extern uint16_t tot_lift;
extern int8_t tot_roll;
extern int8_t tot_pitch;
extern int8_t tot_yaw;

//Static command trims
extern uint16_t lift_trim;
extern int8_t roll_trim;
extern int8_t pitch_trim;
extern int8_t yaw_trim;

//Control parameters
extern uint16_t p;
extern uint16_t p1;
extern uint16_t p2;
extern uint16_t c1;
extern uint16_t c2;

extern int mode;

//Joystick
extern uint16_t lift;
extern int8_t roll;
extern int8_t pitch;
extern int8_t yaw;

extern bool joystick_connected;
extern bool joystick_neutral;

//Fpga
extern bool fpga_connected;
extern int fpga_state;

//QR status
extern int avg_control_loop_time;
extern int max_control_loop_time;
extern int avg_system_response;
extern int max_system_response;
extern uint16_t loop_misses;
extern int16_t roll_response;
extern int16_t pitch_response;
extern int16_t yaw_response;
extern uint16_t ae1;
extern uint16_t ae2;
extern uint16_t ae3;
extern uint16_t ae4;

#define P_INIT 50
#define P1_INIT 50
#define P2_INIT 50
#define C1_INIT 100
#define C2_INIT 10000

void load_status();
void save_status();
int8_t trim_up(int8_t trim);
int8_t trim_down(int8_t trim);
void lift_trim_up();
void lift_trim_down();
uint16_t param_up(uint16_t par);
uint16_t param_down(uint16_t par);
#endif
