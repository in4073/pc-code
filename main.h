#ifndef MAIN_H
#define MAIN_H

#define COMMAND_PERIOD 50 //ms

#define SAFE_MODE 0
#define PANIC_MODE 1
#define MANUAL_MODE 2
#define CALIBRATION_MODE 3
#define YAW_CONTROL_MODE 4
#define FULL_CONTROL_MODE 5
#define EXIT_MODE 7 //Exit state on the FPGA

#define DEBUG 1
#endif
