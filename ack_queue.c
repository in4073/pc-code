/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/

#include <stdlib.h>
#include "protocol.h"
#include "communication.h"
#include "gui.h"

/*------------------------------------------------------------
* The queue can contain only one message for every type, because only the most
* recent is relevant. There are only 2 kinds of messages that must be
* acknowledged by the fpga: MODE and PARAM.
* Control messages don't need to be acknowledged because they are periodic.
*------------------------------------------------------------
*/

Message* queue[MESSAGE_TYPES + 1] = { NULL };


/*__Author: Eric Camellini__
Puts a message in the correct ack waiting slot determined by its type*/
void insert_into_ack_queue(Message *m){
	queue[m->type] = m;
}


/*__Author: Eric Camellini__
Removes a message of a specific type from the queue, because the related
acknowledgement arrived.*/
Message *acknowledge(uint8_t type){
  Message *m = queue[type];
  queue[type] = NULL;
  return m;
}


/*__Author: Eric Camellini__
Returns 1 if there are no messages that must be acknowledged.*/
int is_ack_queue_empty(){
  int i;
  for(i = 1; i < MESSAGE_TYPES; i++){
    if(queue[i] != NULL){
      return 0;
		}
	}
  return 1;
}


/*__Author: Eric Camellini__
Resends all the unacknowledged messages.*/
void resend_ack_queue(){
  if(!is_ack_queue_empty()){
    print_scroll("Resending ack queue.", GREEN_BLACK);
    int i;
    for(i = 1; i < MESSAGE_TYPES; i++){
      if(queue[i] != NULL){
        send_message(queue[i], 0);
			}
		}
  }
}


/*__Author: Eric Camellini__
Prints the messages that are in the ack queue.*/
void print_ack_queue() {
  int i;
  for(i = 1; i < MESSAGE_TYPES; i++) {
    if(queue[i] != NULL){
      dump_msg(queue[i]);
		}
  }
}
