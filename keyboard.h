#ifndef KEYBOARD_H
#define KEYBOARD_H

#define KB_CHANGE_COMMAND 1 //How much the offset changes each keyboard hit for commands
#define KB_CHANGE_PARAMS 1 //How much the offset changes each keyboard hit for parameter settings

void handle_keyboard_input(int ch);
void end();

#endif
