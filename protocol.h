#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdint.h>

#define SEPARATOR 'M'

#define MESSAGE_TYPES 10

#define MESSAGE_CONTROL 1
#define MESSAGE_MODE 2
#define MESSAGE_PARAM 3
#define MESSAGE_DEBUG 4
#define MESSAGE_TELEMETRY 5
#define MESSAGE_ACK 6
#define MESSAGE_START_LOG 7
#define MESSAGE_REQUEST_LOG 8
#define MESSAGE_LOG_SENSORS 9
#define MESSAGE_LOG_TELEMETRY 10
//define message types...

#define ACK_TIMEOUT 100 //milliseconds

extern int payload_size[MESSAGE_TYPES + 1];

//Messages to send
typedef struct {
  //int8_t separator;
  //int8_t sequence_number;
  unsigned char type;
  void *payload;
  unsigned char check;
} Message;

#define PAYLOAD_CONTROL_SIZE 5
typedef struct {
  uint16_t lift;
  int8_t roll;
  int8_t pitch;
  int8_t yaw;
} PayloadControl;

#define PAYLOAD_MODE_SIZE 1
typedef struct {
  uint8_t mode;
} PayloadMode;

#define PAYLOAD_PARAM_SIZE 10
typedef struct {
  uint16_t p;
  uint16_t p1;
  uint16_t p2;
  uint16_t c1;
  uint16_t c2;
} PayloadParam;

//Messages to receive
#define PAYLOAD_START_LOG_SIZE 0
#define PAYLOAD_REQUEST_LOG_SIZE 0

#define PAYLOAD_DEBUG_SIZE 30
typedef struct {
  char debug[PAYLOAD_DEBUG_SIZE];
} PayloadDebug;

#define PAYLOAD_ACK_SIZE 1
typedef struct {
  uint8_t ack_type;
} PayloadAck;

#define PAYLOAD_LOG_SENSORS_SIZE 28
typedef struct {
  uint32_t timestamp;
  int32_t s1;
  int32_t s2;
  int32_t s3;
  int32_t s4;
  int32_t s5;
  int32_t s6;
} PayloadLogSensors;

#define PAYLOAD_LOG_TELEMETRY_SIZE 14
typedef struct {
  uint32_t timestamp;
  uint16_t rec_lift;
  int8_t rec_roll;
  int8_t rec_pitch;
  int8_t rec_yaw;
  int8_t mode;
  int8_t ae1;
  int8_t ae2;
  int8_t ae3;
  int8_t ae4;
} PayloadLogTelemetry;

#define PAYLOAD_TELEMETRY_SIZE 26
typedef struct {
  unsigned int avg_loop_time;
  unsigned int max_loop_time;
  uint16_t state;
  uint16_t loop_misses;
  uint16_t roll_response;
  uint16_t pitch_response;
  uint16_t yaw_response;
  uint16_t ae1;
  uint16_t ae2;
  uint16_t ae3;
  uint16_t ae4;
} PayloadTelemetry;

void dump_msg(Message *m);
Message *new_message(uint8_t type, void *payload);
unsigned char calculate_checksum(Message *m);
PayloadControl *new_payload_control(uint16_t lift, int8_t roll, int8_t pitch, int8_t yaw);
PayloadMode *new_payload_mode(uint8_t mode);
PayloadParam *new_payload_param(uint16_t p, uint16_t p1, uint16_t p2, uint16_t c1, uint16_t c2);

#endif
