#ifndef SENDER_H
#define SENDER_H

void send_params();
void change_mode(uint8_t md);
void send_commands();
void start_logging();
void log_request();

#endif
