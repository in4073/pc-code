/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/

#include <stdlib.h>
#include "protocol.h"
#include "communication.h"
#include "status.h"
#include "gui.h"

uint16_t tot_lift = 0;
int8_t tot_roll = 0;
int8_t tot_pitch = 0;
int8_t tot_yaw = 0;

/*__Author: Eric Camellini__
Sends a message with the current control paramters. */
void send_params(){
	PayloadParam *payload = new_payload_param(p, p1, p2, c1, c2);
	Message *m = new_message(MESSAGE_PARAM, payload);
	//dump_msg(m);
	send_message(m, ACK);
}


/*__Author: Eric Camellini__
Sends a mode change message. */
void change_mode(uint8_t md){
	mode = md;
	PayloadMode *payload = new_payload_mode(md);
	Message *m = new_message(MESSAGE_MODE, payload);
	//dump_msg(m);
	send_message(m, ACK);
}


/*__Author: Eric Camellini__
Send a message with the current lift, roll, pitch, yaw. */
void send_commands(){
	uint16_t u;
	((lift + lift_trim > MAX_LIFT) ? u = MAX_LIFT : ((lift + lift_trim < MIN_LIFT) ? u = MIN_LIFT : (u = lift + lift_trim)));
	tot_lift = u;
	int8_t i;
	((roll + roll_trim > MAX_COMMAND) ? i = MAX_COMMAND : ((roll + roll_trim < MIN_COMMAND) ? i = MIN_COMMAND : (i = roll + roll_trim)));
	tot_roll = i;
	((pitch + pitch_trim > MAX_COMMAND) ? i = MAX_COMMAND : ((pitch + pitch_trim < MIN_COMMAND) ? i = MIN_COMMAND : (i = pitch + pitch_trim)));
	tot_pitch = i;
	((yaw + yaw_trim > MAX_COMMAND) ? i = MAX_COMMAND : ((yaw + yaw_trim < MIN_COMMAND) ? i = MIN_COMMAND : (i = yaw + yaw_trim)));
	tot_yaw = i;

	PayloadControl *payload = new_payload_control(tot_lift, roll + roll_trim, pitch + pitch_trim, yaw + yaw_trim);
	Message *m = new_message(MESSAGE_CONTROL, payload);
	//dump_msg(m);
	send_message(m, NO_ACK); //Periodic
}


/*__Author: Eric Camellini__
Sends a message to ask the fpga to start logging. */
void start_logging(){
	Message *m = new_message(MESSAGE_START_LOG, NULL);
	print_scroll("Sending LOG START message.", GREEN_BLACK);
	send_message(m, ACK);
}


/*__Author: Eric Camellini__
Sends a message to request the log download action. */
void log_request(){
	Message *m = new_message(MESSAGE_REQUEST_LOG, NULL);
	print_scroll("Sending LOG REQUEST message.", GREEN_BLACK);
	send_message(m, ACK);
}
