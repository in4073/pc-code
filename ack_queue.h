#ifndef ACK_QUEUE_H
#define ACK_QUEUE_H

#include "protocol.h"

void insert_into_ack_queue(Message *m);
Message *acknowledge();
void resend_ack_queue();
int is_ack_queue_empty();
void print_ack_queue();

#endif
