/*------------------------------------------------------------
 * AUTHOR: Eric Camellini
 *------------------------------------------------------------
 */

#include <stdlib.h>
#include "protocol.h"
#include "ack_queue.h"
#include "logger.h"
#include "gui.h"
#include "status.h"
#include "main.h"

int avg_control_loop_time = 0;
int max_control_loop_time = 0;
uint16_t loop_misses = 0;
int16_t roll_response = 0;
int16_t pitch_response = 0;
int16_t yaw_response = 0;
uint16_t ae1 = 0;
uint16_t ae2 = 0;
uint16_t ae3 = 0;
uint16_t ae4 = 0;
int fpga_state = 0;

char *str;


/*__Author: Eric Camellini__
Updates the telemetry data with the most recent ones received by the fpga. */
void update_telemetry(PayloadTelemetry* payload){
	fpga_state = payload->state;
	if(fpga_state >= 0 && fpga_state <= 5){
		mode = fpga_state;
	}

	avg_control_loop_time = payload->avg_loop_time;
	max_control_loop_time =  payload->max_loop_time;
	loop_misses = payload->loop_misses;
	roll_response = payload->roll_response;
	pitch_response = payload->pitch_response;
	yaw_response = payload->yaw_response;
	ae1 = payload->ae1;
	ae2 = payload->ae2;
	ae3 = payload->ae3;
	ae4 = payload->ae4;
}


/*__Author: Eric Camellini__
Removes an acknowledged message from the ack queue. */
void handle_ack(Message *m){
	Message *acked = acknowledge(((PayloadAck *) m->payload)->ack_type);
	if(acked != NULL){
		print_scroll("Ack received for msg type %d.", GREEN_BLACK, acked->type);
		//dump_msg(acked);
	}else{
		print_scroll("Unexpected ack, ignored.", YELLOW_BLACK);
	}
}


/*__Author: Eric Camellini__
Calls the handler for a certain message type. */
void handle_message(Message *m){

	switch (m->type) {

		case MESSAGE_TELEMETRY:
			update_telemetry((PayloadTelemetry *) m->payload);
			break;

		case MESSAGE_ACK:
		  handle_ack(m);
			break;

		case MESSAGE_LOG_SENSORS:
			log_sensors_line((PayloadLogSensors *) m->payload);
			break;

		case MESSAGE_LOG_TELEMETRY:
			log_telemetry_line((PayloadLogTelemetry *) m->payload);
			break;

		case MESSAGE_DEBUG:
			if(DEBUG){
		  	str = ((char *) m->payload);
				print_scroll(str, CYAN_BLACK);
			}

		default:
			//print_scroll("Received message of type %d, handler undefined.", m->type);
			break;
	}
}
